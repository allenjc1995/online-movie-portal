var express = require('express');
var router = express.Router();
var ipfsClient = require('ipfs-http-client');
const nav = [
  {
      link:'/artist',name:'Artists' 
  },
  {
      link:'/Gallery',name:'Gallery'
  },
  {
      link:'/login',name:'Login'
  }
  
];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { nav:[{link:'/artist',name:'Art Upload'},{link:'/gallery',name:'Gallery'},{link:'/login',name:'Login'}],
                         title: 'Art & Blockchain' });
});

router.get('/artist', function(req, res, next) {
  res.render('artist', { nav:[{link:'/artist',name:'Art Upload'},{link:'/gallery',name:'Gallery'},{link:'/login',name:'Login'}],
                         title: 'Artwork Upload' });
});

router.get('/gallery', function(req, res, next) {
  res.render('gallery', { nav:[{link:'/artist',name:'Art Upload'},{link:'/gallery',name:'Gallery'},{link:'/login',name:'Login'}],
                         title: 'Gallery' });
});

router.get('/login', function(req, res, next) {
  res.render('login', { nav:[{link:'/artist',name:'Art Upload'},{link:'/gallery',name:'Gallery'},{link:'/login',name:'Login'}],
                         title: 'Login' });
});



router.post('/uploadArtwork', async function (req, res, next) {
  uid = req.body.uid;
  data = req.body;
  console.log(data);  
  fileBytes = req.files.Artwork.data;

  const ipfs = ipfsClient('/ip4/127.0.0.1/tcp/5001');

  const result = await ipfs.add(fileBytes)
  console.log(result.path);
  web3.eth.getAccounts().then((accounts)=>{
  SMS.methods.setImageHash(uid,web3.utils.toHex(result.path),data.artistname,data.age,data.artistlocation,data.artistaddress).send({ from: coinbase,gas:410000 }).then((txn) => {
        res.send(txn);
  });
});
});

router.get('/viewArtwork', function (req, res, next) {
  data = req.query;
  console.log(data);
  web3.eth.getAccounts().then((accounts)=>{
  SMS.methods.getImageHash(data.uid)
    .call({ from: coinbase }).then((val) => {
      console.log(val);
      ipfspath =  web3.utils.hexToAscii(val[0]);
      console.log(ipfspath);
      // res.redirect(ipfspath);
      res.render("gallery",{ nav:[{link:'/artist',name:'Art Upload'},{link:'/gallery',name:'Gallery'},{link:'/login',name:'Login'},{link:'/signup',name:'signup'}],
      title : "Gallery" , result : val , imagepath : ipfspath })
    });                                       
});
});

router.post('/addUser', function(req, res, next) {
  data = req.body;
  console.log(data);
  web3.eth.getAccounts().then((accounts)=>{
  SMS.methods.enrollUser(data.uid, data.name,data.location )
    .send({from:accounts[0],gas:400000})
    .then((txn) => {
      console.log(txn);
        res.send(txn);
    })
});
});

router.post('/reward', function(req, res, next) {
  data = req.body;
  console.log(data);
  web3.eth.getAccounts().then((accounts)=>{  
  SMS.methods.reward(data.studentAddress, data.amount)
    .send({from:accounts[0]})
    .then((txn) => {
        res.send(txn);
    })
});
});

module.exports = router;
