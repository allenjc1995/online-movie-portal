// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

import "./IERC20.sol";
import "./SafeMath.sol";


contract Artgallery is IERC20 {
    using SafeMath for uint256;

    mapping (address => uint256) private _balances;
    mapping (address => mapping (address => uint256)) private _allowances;

    uint256 private _totalSupply;
  
    string private _name;
    string private _symbol;
    uint8 private _decimals;
    address private _admin;
    
    constructor (string memory name, string memory symbol,uint8 decimal) {
        _name = name;
        _symbol = symbol;
        _decimals = decimal;
        _admin = msg.sender;
    }

    /* -----------Core ERC20 Functions ------------*/
    
    function name() public view returns (string memory) {
        return _name;
    }

    function symbol() public view returns (string memory) {
        return _symbol;
    }

    function decimals() public view returns (uint8) {
        return _decimals;
    }

    function totalSupply() override public view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) override public view returns (uint256) {
        return _balances[account];
    }

    function transfer(address recipient, uint256 amount) override public returns (bool) {
        _transfer(msg.sender, recipient, amount);
        return true;
    }

    function allowance(address owner, address spender) override public view returns (uint256) {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount) override public returns (bool) {
        _approve(msg.sender, spender, amount);
        return true;
    }

    function transferFrom(address sender, address recipient, uint256 amount) override public returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, msg.sender, _allowances[sender][msg.sender].sub(amount, "ERC20: transfer amount exceeds allowance"));
        return true;
    }

    /* ------------------------- */
    
    function _transfer(address sender, address recipient, uint256 amount) internal {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");

        _balances[sender] = _balances[sender].sub(amount, "ERC20: transfer amount exceeds balance");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    function _mint(address account, uint256 amount) internal {
        require(account != address(0), "ERC20: mint to the zero address");

        _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }

    function _approve(address owner, address spender, uint256 amount) internal {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }
    
    /*-------------------------*/ 
    
    /*------------Custom Function and State variables--------*/

    struct artist{
        string artistname;
        uint age;
        string artistlocation;
        bytes imageHash;
        address artistaddress;
    }
    
    mapping (uint => artist) profile;
    mapping (uint => string) public users;
    
    
    function enrollUser(uint _id, string memory _uname, string memory _ulocation ) public {   //function to enroll user
        users[_id] = _uname;
        users[_id] = _ulocation;
        _mint(_admin, 1000);
    }

    
    
    function reward(address _artistAddress, uint _amount) public {     //function to give tokens to artist
        transferFrom(_admin, _artistAddress, _amount);
    }
        function setImageHash(uint _uid,bytes memory _imageHash,string memory _artistname,uint _age,string memory _artistlocation,address _artistaddress) public  {    //function to upload artwork

        profile[_uid] = artist(_artistname,_age,_artistlocation,_imageHash,_artistaddress);
        
    }

    function getImageHash(uint _uid) view public returns(bytes memory,string memory,uint,string memory,address){    //function to retrieve artwork 
        return (
           profile[_uid].imageHash,
           profile[_uid].artistname,
           profile[_uid].age,
           profile[_uid].artistlocation,
           profile[_uid].artistaddress
        );
    }

    /*-------------------------*/

}