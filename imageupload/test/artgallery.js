const Artgallery = artifacts.require("Artgallery");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("Artgallery", function (/* accounts */) {
  it("should assert true", async function () { 
    await Artgallery.deployed();
    return assert.isTrue(true);
  });
  it("add artwork Details", async function () {  
    const artgallery= await Artgallery.deployed();  //contract deployment

       uid = 10001;  
       imageHash = "0x416c6c656e";
       artistname = "Allen";
       age = 25;
       artistlocation = "TVM";
       
       
            await artgallery.setImageHash(uid , imageHash , artistname , age , artistlocation); 
            data = await artgallery.getImageHash(uid);
            assert.equal(data[0], imageHash, "Test Fail");
            assert.equal(data[1], artistname, "Test Fail");
            assert.equal(data[2], age, "Test Fail");
            assert.equal(data[3], artistlocation, "Test Fail");
});
});